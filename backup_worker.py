######################################################################################################################
#  SAMPLE CODE for DATA ARCHIVING FROM RDS & S3
#    Version: 1.0.171120
#    Updated at: Nov 20, 2017
#    Created at: Oct 27, 2017
#    Author:  Sze Chern Tan
######################################################################################################################
import boto3
import botocore
import datetime
import subprocess
import tarfile
import sys
import os
import os.path
import time
from dotenv import load_dotenv
import urllib

# SET PROXY CONFIG FOR BOTO
try:
    http_proxy = os.environ.get("http_proxy")
except TypeError as e:
    print("http_proxy not found in environment variables. HTTP proxy will not be used.")
    http_proxy =""

try:
    https_proxy = os.environ.get("https_proxy")
except TypeError as e:
    print("https_proxy not found in environment variables. HTTPS proxy will not be used.")
    https_proxy =""

proxyConfig = botocore.config.Config(proxies={'http': http_proxy, 'https': https_proxy})

# BOTO CLIENTS and RESOURCES
ddb_client = boto3.resource('dynamodb', config=proxyConfig)
ssm_client = boto3.client('ssm', config=proxyConfig)
s3_client = boto3.client('s3', config=proxyConfig)
sns_client = boto3.client('sns', config=proxyConfig)
sqs_client = boto3.client('sqs', config=proxyConfig)
glacier_client = boto3.client('glacier', config=proxyConfig)
rds_client = boto3.client('rds', config=proxyConfig)

# Load program global variables from .env into environment variables
dotenv_path = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(dotenv_path,verbose=True)

# ENVIRONMENT VARIABLES
prefix_local_working_dir = os.environ.get("prefix_local_working_dir")
prefix_local_log_dir=prefix_local_working_dir+"/logs"
log_bucket=os.environ.get("log_bucket")
ddb_history_table_name=os.environ.get("ddb_history_table_name")
backup_job_queue=os.environ.get("backup_job_queue")
backup_sns_topic=os.environ.get("backup_sns_topic")
db_backup_user=os.environ.get("db_backup_user")
ssm_db_backup_parameter=os.environ.get("ssm_db_backup_parameter")

# Local variables
instance_id = urllib.urlopen('http://169.254.169.254/latest/meta-data/instance-id').read().decode()
backupworker_logfile_str=""
rds_cert="/eap/code/backupworker/rds-combined-ca-bundle.pem"


def uploadLogsToS3(file_location, file_name):
    writeLog("Writing file "+file_location+" to S3 bucket "+log_bucket)

    with open(file_location, 'rb') as data:
        s3_client.upload_fileobj(data, log_bucket, file_name)


def updateBackupHistoryTable(backup_id, status):
    timestamp = getCurrentUTCTimeAsString()

    history_table=ddb_client.Table(ddb_history_table_name)

    response=history_table.update_item(
        Key={'Backup_Id': backup_id},
        UpdateExpression="set Archive_Status = :s, Last_Update = :t, Worker_Id = :u",
        ExpressionAttributeValues={
            ':s':status,
            ':t':timestamp,
            ':u':instance_id
        }
    )


def completeBackupHistoryTable(backup_id, vault_id, archive_id, archive_filename, checksum, retention_in_days):
    timestamp = getCurrentUTCTimeAsString()
    expiry_date = str(calculatePurgeDate(retention_in_days))

    history_table=ddb_client.Table(ddb_history_table_name)

    response=history_table.update_item(
        Key={'Backup_Id': backup_id},
        UpdateExpression="set Archive_Status = :s, Last_Update = :t, Archive_Id = :u, Vault_Id = :v, Archive_Name = :w, Archive_SHA256Hash = :x, Expiry_Date = :y",
        ExpressionAttributeValues={
            ':s':"COMPLETE",
            ':t':timestamp,
            ':u':archive_id,
            ':v':vault_id,
            ':w':archive_filename,
            ':x':checksum,
            ':y':expiry_date
        }
    )



def callPgDumpAll(backup_id, db_conn_string,local_backup_dir):

    dump_name=local_backup_dir+"/"+backup_id+"-globals.sql"

    writeLog(("Dumping database global objects (roles & tablespaces) to local file: %s" % (dump_name)))

    manifest_dir = prefix_local_log_dir+"/DB/"
    createSubDirectories(manifest_dir)

    manifest_file_name=backup_id+"-pg_dumpall.manifest"
    manifest_file_fullpath=manifest_dir+manifest_file_name

    writeLog("Piping outputs of pg_dumpall shell command to "+manifest_file_fullpath)

    with open(manifest_file_fullpath, 'w+') as f:
        p = subprocess.Popen(["pg_dumpall","--globals-only", "--no-role-password","-d",db_conn_string,"-f",dump_name],
                            stdout=f, stderr=f)
        output, err = p.communicate()

    uploadLogsToS3(manifest_file_fullpath, ("DB/"+manifest_file_name))

    return p.returncode



def backupDatabase(backup_id, source_id, db_conn_string,local_backup_dir):

    dump_name=local_backup_dir+"/"+backup_id+".dump"

    writeLog(("Dumping database %s to local file: %s" % (source_id, dump_name)))

    manifest_dir = prefix_local_log_dir+"/DB/"
    createSubDirectories(manifest_dir)

    manifest_file_name=backup_id+"-pg_dump.manifest"
    manifest_file_fullpath=manifest_dir+manifest_file_name

    writeLog("Piping output of pg_dump shell command to "+manifest_file_fullpath)

    with open(manifest_file_fullpath, 'w+') as f:
        p = subprocess.Popen(["pg_dump","-v","-Fc",db_conn_string,"--file",dump_name],
                         stdout=f, stderr=f)
        output, err = p.communicate()

    uploadLogsToS3(manifest_file_fullpath, ("DB/"+manifest_file_name))

    return p.returncode


def getRDSPostgresConnectionString(source_id):
    ssm_response = ssm_client.get_parameter(Name=ssm_db_backup_parameter,WithDecryption=True)
    ssm_parameter = ssm_response['Parameter']
    db_passwd = str(ssm_parameter['Value'])

    response = rds_client.describe_db_instances(DBInstanceIdentifier=source_id)

    rds_endpoint = response['DBInstances'][0]['Endpoint']
    db_hostname = rds_endpoint['Address']
    db_port = rds_endpoint['Port']

    db_conn_string="postgresql://%s:%s@%s:%s" % (db_backup_user, db_passwd, db_hostname, db_port)

    return db_conn_string


def checkBucketExists(bucket_name_str):
    try:
        response=s3_client.head_bucket(Bucket=bucket_name_str)
        s3BucketExists = response['ResponseMetadata']['HTTPStatusCode']
    except botocore.exceptions.ClientError:
        return False

    return True


def backupS3Bucket(backup_id, bucket_name, dump_directory):
    num_s3_objs=-1

    num_s3_objs=0

    writeLog("Taking backup of S3 bucket "+bucket_name)

    # Create directory to store backup dumps, if it doesn't exist
    if not os.path.exists(dump_directory):
        os.makedirs(dump_directory)
        writeLog("Created directory to store backup: %s" % (dump_directory))
    else:
        writeLog("Directory already exists. Skipping creation of directory %s" %(dump_directory))

    objectList = s3_client.list_objects_v2(Bucket=bucket_name)

    # Create directory to store backup objects, if it doesn't exist
    manifest_dir = prefix_local_log_dir+"/S3/"
    createSubDirectories(manifest_dir)

    # List of objects backed up from this S3 bucket will be stored in a plain text manifest file
    manifest_file_name=backup_id+".manifest"
    manifest_file_fullpath=manifest_dir+manifest_file_name

    writeLog("Recording content of files backed up in: "+manifest_file_fullpath)

    # Copy objects in source bucket to local filesystem
    num_s3_objs = copyBucketToLocalFS(objectList, bucket_name, dump_directory,manifest_file_fullpath)
    writeLog(("Backed up %s object(s) from %s in %s" % (num_s3_objs, bucket_name, dump_directory)))

    uploadLogsToS3(manifest_file_fullpath, ("S3/"+manifest_file_name))

    return num_s3_objs


def getCurrentUTCTimeAsString():
    return str(datetime.datetime.utcnow().strftime("%Y-%h-%dT%H:%MZ"))


def createS3Subdirectory(file_path):
    subdirs=file_path.split("/")
    subdirs.pop()
    sub_directory_str="/".join(subdirs)
    createSubDirectories(sub_directory_str)


def createSubDirectories(file_path):
    subdirs=file_path.split("/")
    sub_directory_str="/".join(subdirs)
    if not os.path.exists(sub_directory_str):
        os.makedirs(sub_directory_str)


def copyBucketToLocalFS(object_list, src_bucket_name, dump_directory,s3_manifest_filename):
    numKeys=0

    for key in object_list['Contents']:
        local_file_name=dump_directory+"/"+key['Key']
        object_type = boto3.resource('s3').Bucket(src_bucket_name).Object(key['Key']).content_type

        if object_type == 'application/x-directory':
            createS3Subdirectory(local_file_name)
        else:
            createS3Subdirectory(local_file_name)
            with open(local_file_name, 'wb') as data:
                s3_client.download_fileobj(src_bucket_name, key['Key'],data)
            message="\n"+key['Key']+","+str(key['LastModified'])+","+str(key['Size'])
            writeBackupManifest(message,s3_manifest_filename)

        numKeys+=1

    try:
        # list_objects_v2 only returns 1000 items max; check if there are additional objects in the Bucket...
        token = object_list['NextContinuationToken']
        next_object_list = s3_client.list_objects_v2(Bucket=src_bucket_name, ContinuationToken=token)

        #... and recursively call this method again to continue backups
        numCopied = copyBucketToLocalFS(next_object_list, src_bucket_name, dump_directory,s3_manifest_filename)
        numKeys+=numCopied
    except KeyError:
        exit

    return numKeys


def writeBackupManifest(message, manifest_file_str):
    if not os.path.exists(manifest_file_str):
        logFile = open(manifest_file_str,"w")
        headermsg="\nManifest file created for backup job: "+backup_id
        logFile.write(message)
    else:
        logFile = open(manifest_file_str,"a")
        logFile.write(message)


def writeS3Manifest(src_object_key, src_object_lastmodified, src_object_size, s3_manifest_file):
    message="\n"+src_object_key+","+src_object_lastmodified+","+src_object_size

    if not os.path.exists(s3_manifest_file):
        logFile = open(s3_manifest_file,"w")
        headermsg="\nSourceBucket,ObjectName,LastModified,Size,Checksum"
        logFile.write(message)
    else:
        logFile = open(s3_manifest_file,"a")
        logFile.write(message)


def writeLog(message):
    output = "\n"+instance_id+": ["+getCurrentUTCTimeAsString()+"] "+message

    backupworker_dir_str=prefix_local_log_dir

    createSubDirectories(backupworker_dir_str)

    backupworker_logfile_str=backupworker_dir_str+"/"+instance_id+"_backupworker"+".log"

    if not os.path.exists(backupworker_logfile_str):
        logFile = open(backupworker_logfile_str,"w")
        logFile.write(output)
    else:
        logFile = open(backupworker_logfile_str,"a")
        logFile.write(output)

    print(output)

def createArchive(backup_id, fullFilePath):
    writeLog("Zipping up this directory: "+fullFilePath)

    subdirs=fullFilePath.split("/")
    subdirs.pop()
    archive_path="/".join(subdirs)
    archive_path+="/archive/"

    createSubDirectories(archive_path)

    tar_file_name=archive_path+backup_id+".tgz"

    with tarfile.open( tar_file_name, "w:gz" ) as tar:
        for name in os.listdir( fullFilePath ):
            tar.add(fullFilePath+"/"+name)

    return tar_file_name


def uploadArchiveToGlacier(vault_id, archive_file):
    writeLog("Uploading file "+archive_file+" to Glacier Vault "+vault_id)

    with open(archive_file, 'rb') as data:
        archive_id = glacier_client.upload_archive(
            vaultName=vault_id,
            body=data
        )

    return archive_id


def calculatePurgeDate(retention_in_days):
    return datetime.datetime.utcnow()+datetime.timedelta(days=int(retention_in_days))


def calculateChecksum(filename):
    return botocore.utils.calculate_tree_hash(open(filename))


if __name__ == '__main__':
    # 1) Get a backup job off SQS queue; set message visibility = 43200 (12 hours; SQS max)
    # 2) Create an entry in History table: set WorkerId (hostname), status=BACKUP_STARTING
    # 3) Get Backup Policy from policy table
    #       If Source Type = Postgres
    #           Get DB connection string from Policy table + SSM parameter store
    #           Call Database backup (get dump file name and location)
    #       Else If Source Type = S3
    #           Call S3 backup (get dump filename and location)
    # 4) Update History table; set status=ARCHIVE_PENDING + Remove message from backup job queue
    # 5) Archive backup to Glacier
    # 6) Update History table; set status=COMPLETE
    # 7) Remove backup job from SQS queue
    # on error: notify backup_sns_topic

    while True:

        backup_jobs=sqs_client.receive_message(
            QueueUrl=backup_job_queue,
            MaxNumberOfMessages=1,
            VisibilityTimeout=100
        )

        try:
            for message in backup_jobs["Messages"]:

                backupComplete = False

                message_receipt = message['ReceiptHandle']
                backup_job_info=message['Body'].split(",")

                backup_id = backup_job_info[0]
                app_id = backup_job_info[1]
                source_type = backup_job_info[2]
                source_id = backup_job_info[3]
                backup_scope = backup_job_info[4]
                backup_retention = int(backup_job_info[5])
                vault_id = backup_job_info[6]

                backupworker_logfile_str=instance_id+"-"+backup_id+"-"+getCurrentUTCTimeAsString()+".log"
                updateBackupHistoryTable(backup_id, "BACKUP_ASSIGNED")

                writeLog("Creating a local backup of "+backup_scope+" from "+source_id+" for "+app_id)

                if source_type == "DB":
                    updateBackupHistoryTable(backup_id, "BACKUP_STARTING")

                    #TODO add a try catch block here in case DB information is missing from Policy table
                    ssm_response = ssm_client.get_parameter(Name=ssm_db_backup_parameter,WithDecryption=True)
                    ssm_parameter = ssm_response['Parameter']
                    db_passwd = str(ssm_parameter['Value'])

                    local_backup_dir = prefix_local_working_dir+"/"+app_id+"/DB/"+backup_id
                    createSubDirectories(local_backup_dir)

                    db_connection_string=getRDSPostgresConnectionString(source_id)

                    # ------------------------------------------------------------------------------------------------------------------------------------
                    #   Comment out the next 2 rows of code if pg_dumpall is not required/supported (requires Postgres10 client to run pg_dumpall of RDS)
                    # ------------------------------------------------------------------------------------------------------------------------------------
                    pg_dumpall_conn=db_connection_string+"?sslmode=verify-full&sslrootcert="+rds_cert
                    backupGlobals = callPgDumpAll(backup_id, pg_dumpall_conn, local_backup_dir)

                    pg_dump_conn="--dbname="+db_connection_string
                    pg_dump_conn+="/"
                    pg_dump_conn+=backup_scope
                    pg_dump_conn+="?sslmode=verify-full&sslrootcert="+rds_cert
                    backupDB = backupDatabase(backup_id, source_id, pg_dump_conn,local_backup_dir)

                    if backupGlobals != 0 or backupDB != 0:
                        writeLog("Error when creating DB dump")
                        updateBackupHistoryTable(backup_id, "BACKUP_ERROR")
                        sns_message_subject="[BACKUP ERROR] Postgres DB dump error on DB "+backup_scope+" in "+source_id
                        sns_message_body="\nDB backup attempted by Backup Worker on "+instance_id+" at "+getCurrentUTCTimeAsString()
                        sns_message_body="\npg_dumpall of global objects in "+source_id+" exited with code: "+str(backupGlobals)
                        sns_message_body+="\npg_dump of "+backup_scope+" exited with code: "+str(backupDB)
                        sns_message_body+="\nNon-zero status code indicates an error."
                        sns_client.publish(TopicArn=backup_sns_topic, Subject=sns_message_subject,Message=sns_message_body)

                    else:
                        writeLog("Dump taken")
                        updateBackupHistoryTable(backup_id, "ARCHIVE_PENDING")
                        backupComplete = True

                elif source_type == "S3":
                    local_backup_dir = prefix_local_working_dir+"/"+app_id+"/S3/"+backup_id
                    createSubDirectories(local_backup_dir)



                    if checkBucketExists(source_id):
                        writeLog("Backing up S3 objects to local filesystem: "+local_backup_dir)
                        numObjs = backupS3Bucket(backup_id, source_id, local_backup_dir)
                        writeLog("Copied "+str(numObjs)+" objects from S3 bucket "+source_id)

                        if numObjs > 0:
                            updateBackupHistoryTable(backup_id, "ARCHIVE_PENDING")
                            backupComplete = True
                        else:
                            writeLog("Did not backup any objects from "+source_id)
                    else:
                        writeLog("Could not find bucket "+source_id)

                if backupComplete:
                    writeLog("Creating compressed tarball of the backup dump directory")
                    archive_file = createArchive(backup_id, local_backup_dir)
                    archive_checksum = calculateChecksum(archive_file)

                    writeLog("Backup archive file path = "+archive_file)
                    writeLog("Backup archive file checksum = "+archive_checksum)

                    # Upload into Glacier
                    writeLog("Uploading compressed tarball to Glacier vault "+vault_id)
                    glacier_response = uploadArchiveToGlacier(vault_id, archive_file)
                    archive_id = glacier_response["archiveId"]

                    # Close off the backup job in the history table and remove from the SQS queue
                    completeBackupHistoryTable(backup_id, vault_id, archive_id, archive_file, archive_checksum, backup_retention)
                    writeLog("Archive uploaded to Glacier; archive ID = "+archive_id)
                    sqs_client.delete_message(QueueUrl=backup_job_queue, ReceiptHandle=message_receipt)

                    # Send an email to the Backup Ops SNS topic upon successful backup completion
                    sns_message_subject="[BACKUP COMPLETE] "

                    if source_type == "DB":
                        sns_message_subject+=" Database "+backup_scope+" from "+source_id
                    elif source_type == "S3":
                        sns_message_subject+=" S3 Bucket "+source_id

                    sns_message_body="\nBackup completed by "+instance_id+" at "+getCurrentUTCTimeAsString()
                    sns_message_body+="\nData written to "+archive_file
                    sns_message_body+="\nGlacier vault = "+vault_id
                    sns_message_body+="\nArchive ID = "+archive_id
                    sns_client.publish(TopicArn=backup_sns_topic, Subject=sns_message_subject,Message=sns_message_body)

                    # Upload a copy of the latest backupworker log into S3
                    worker_log_full_path = prefix_local_log_dir+"/"+instance_id+"_backupworker"+".log"
                    print("Logs = "+worker_log_full_path)
                    uploadLogsToS3(worker_log_full_path, ("logs/"+instance_id+"_backupworker"+".log"))

        except KeyError as e:
            writeLog("No backup jobs in queue. Sleeping for 30 seconds.")
            exit

        time.sleep(30)
